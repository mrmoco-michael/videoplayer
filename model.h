#ifndef MODEL_H
#define MODEL_H

#include <QString>
#include <QObject>

class Model : public QObject {

    Q_OBJECT

public:
    Model(QString url, QObject* parent = nullptr) : m_url(url)
    {

    }

    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)

    QString url() const { return m_url; }
    void setUrl(const QString &v) {
        m_url = v;
        emit urlChanged();
    }

signals:
    void urlChanged();

private:
    QString m_url;

};

#endif // MODEL_H
