import QtQuick 2.0
import QtMultimedia 5.0

VideoOutput {
    id: root
    height: width
    source: mediaPlayer

    property alias duration: mediaPlayer.duration
    property alias mediaSource: mediaPlayer.source
    property alias metaData: mediaPlayer.metaData
    property alias playbackRate: mediaPlayer.playbackRate
    property alias position: mediaPlayer.position
    property alias volume: mediaPlayer.volume

    signal sizeChanged
    signal fatalError

    onHeightChanged: root.sizeChanged()

    function play() {
        mediaPlayer.play();
        console.log("[qmlvideo] play video");
    }
    function pause() {
        mediaPlayer.pause();
        console.log("[qmlvideo] pause video");
    }
    function stop() {
        mediaPlayer.stop();
        console.log("[qmlvideo] stop video");
    }
    function seek(position) { mediaPlayer.seek(position); }

    MediaPlayer {
        id: mediaPlayer
        autoLoad: false
        loops: Audio.Infinite

        onError: {
            if (MediaPlayer.NoError != error) {
                console.log("[qmlvideo] VideoItem.onError error " + error + " errorString " + errorString)
                root.fatalError()
            }
        }
    }

    onMediaSourceChanged: {
        console.log("[qmlvideo] setting video source to: " + mediaSource);
    }
}
