import QtQuick 2.4
import QtCanvas3D 1.1
import QtQuick.Window 2.2

import "glcode.js" as GLCode

Window {
    title: qsTr("VideoPlayer")
    width: 640
    height: 360
    visible: true

    VideoViewRectangle {
        id: video
        anchors.fill: parent
        focus: true
    }
}
