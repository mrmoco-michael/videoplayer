import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.2
import QtGraphicalEffects 1.0
import QtQuick.Window 2.0
import QtMultimedia 5.4

Item {
    id: rootVideoItem
    anchors.fill: parent

    property string url: model.url

    function play()
    {
        console.log("url: " + url);
        ndiVideo.play();
    }

    function pause()
    {
        ndiVideo.pause();
    }

    function stop()
    {
        ndiVideo.stop();
    }

    Rectangle
    {
        id: innerVideoholdrect
        color: "black"
        anchors.fill: parent

        VideoItem
        {
            id: ndiVideo
            width: parent.width
            height: parent.height
            anchors.fill: parent
            mediaSource: url
        }
    }

    MouseArea
    {
        hoverEnabled: true
        anchors.fill: parent
        visible: true

        // Video player play / stop / pause buttons
        Row
        {
            id: toolbar
            opacity: .55
            height: (rootVideoItem.height * 7) / 100
            spacing: (parent.width * 3) / 100
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.containsMouse ? spacing : -height
            anchors.leftMargin: spacing
            anchors.rightMargin: spacing
            Behavior on anchors.bottomMargin { PropertyAnimation { duration: 250} }
            visible: true

            Rectangle
            {
                id: videoPlayButton
                color:"transparent"

                width: height
                height: parent.height
                visible: true

                Image
                {
                    anchors.fill: parent
                    source: "qrc:/liveStreamPlay.png"
                }

                MouseArea { anchors.fill: parent; onClicked: rootVideoItem.play(); }
            }

            Rectangle
            {
                id: videoPauseButton
                color:"transparent"

                width: height
                height: parent.height

                Image
                {
                    anchors.fill: parent
                    source: "qrc:/liveStreamPause.png"
                }
                MouseArea { anchors.fill: parent; onClicked: { rootVideoItem.pause(); } }
            }

            Rectangle
            {
                id: videoStopButton
                color:"transparent"

                width: height
                height: parent.height

                Image
                {
                    anchors.fill: parent
                    source: "qrc:/liveStreamStop.png"
                }
                MouseArea { anchors.fill: parent; onClicked: rootVideoItem.stop(); }
            }
        }
    }
}
